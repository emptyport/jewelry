(function () {
  // set up the map
	let map = new L.Map('mapid');

	// create the tile layer with correct attribution
	let osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	let osmAttrib='Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
	let osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 12, attribution: osmAttrib});		

	// start the map in South-East England
	map.setView(new L.LatLng(32.8203525, -97.0117366),9);
  map.addLayer(osm);
  
  let marker = L.marker([32.8203525, -97.0117366]).addTo(map);

})();